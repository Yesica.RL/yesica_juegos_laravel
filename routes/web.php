<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InicioController@getInicio');

Route::get('juegos','JuegosController@getTodos');

Route::get('juegos/crear','JuegosController@getCrear');

Route::get('juegos/editar/{id}','JuegosController@getEditar');

Route::get('juegos/ver/{id}','JuegosController@getMostar');

Route::post('juegos/crear','JuegosController@postCrear');

Route::post('juegos/editar/{id}','JuegosController@postEditar');