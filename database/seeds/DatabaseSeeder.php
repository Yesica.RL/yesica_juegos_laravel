<?php

use Illuminate\Database\Seeder;
use App\Juego;
use App\Categoria;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        self::seedCategorias();
        $this->command->info('Tabla Categorias inicializada con datos');
        self::seedJuegos();
        $this->command->info('Tabla Juegos inicializada con datos');
    }
    private function seedCategorias()
    {
    	DB::table('categorias')->delete();
    	$c=new Categoria();

    	$c->categoria='Arcade';
    	$c->save();

    	$c=new Categoria();

    	$c->categoria='Puzzle';
    	$c->save();

    	$c=new Categoria();

    	$c->categoria='Aventuras';
    	$c->save();

    	$c=new Categoria();

    	$c->categoria='Accion';
    	$c->save();

    	$c=new Categoria();

    	$c->categoria='Tablero';
    	$c->save();

    	$c=new Categoria();

    	$c->categoria='Estrategia';
    	$c->save();

    	$c=new Categoria();

    	$c->categoria='Multijugador';
    	$c->save();
    }

    private function seedJuegos()
    {
    	DB::table('Juegos')->delete();

    	$c=Categoria::all()->first();

    	$j=new Juego();
    	$j->titulo="PacMan";
    	$j->descripcion='comecocos que tiene que comer a los fantasmas y consegir mas puntos posibles';
    	$j->votos_positivos=5;
    	$j->votos_negativos=4;
    	$j->imagen='pacMan.jpg';
    	$j->categoria_id=$c->id;
    	$j->save();

    	$j=new Juego();
    	$j->titulo="Ajedrez";
    	$j->descripcion='juego de mesa de logica';
    	$j->votos_positivos=5;
    	$j->votos_negativos=4;
    	$j->imagen='ajedrez.jpg';
    	$j->categoria_id=($c->id +4);
    	$j->save();

    }
}
