<?php 

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Categoria;
	use App\Juego;
	use Illuminate\Support\Facades\Storage;

	class JuegosController extends Controller{

		public function getTodos()
		{
			$juegos=Juego::all();
			return view('juegos.index',array('juegos'=>$juegos));
		}

		public function getCrear()
		{
			$categorias=Categoria::all();
			return view('juegos.crear',array('categorias'=>$categorias));
		}

		public function getEditar($id)
		{
			$juego=Juego::findOrFail($id);
			$categorias=Categoria::all();

			return view('juegos.editar',array('juego'=>$juego,'categorias'=>$categorias));
		}
		public function getMostar($id)
		{
			$juego=Juego::findOrFail($id);
			return view('juegos.mostrar',array('juego'=>$juego));
		}

		public function postCrear(Request $request)
	    {
	    	$juego= new juego();
	    	
			$juego->titulo=$request->titulo;
			$juego->descripcion=$request->descripcion;
			$juego->votos_positivos=$request->votos_positivos;
	    	$juego->votos_negativos=$request->votos_negativos;
	    	$juego->categoria_id=$request->categoria;
	    	$juego->imagen=$request->imagen->store('','juegos');
	    	try{
	    		$juego->save();
	    		return redirect('juegos')->with('mensaje','juego:'. $juego->nombre .'guardada');
	    	}catch(\Illuminate\Database\QueryException $ex){
	    		

	    	}
	    	

	    	return redirect('juegos');
	    }

	    public function postEditar(Request $request)
	    {
	    	$juego= Juego::findOrFail($request->id);
	    	$juego->titulo=$request->titulo;
			$juego->descripcion=$request->descripcion;
			$juego->votos_positivos=$request->votos_positivos;
	    	$juego->votos_negativos=$request->votos_negativos;
	    	$juego->categoria_id=$request->categoria;
	    	if (!empty($request->imagen)&& $request->imagen->isValid()) {
	    		Storage::disk('juegos')->delete($juego->imagen);
	    		$juego->imagen=$request->imagen->store('','juegos');
	    	}
	    	$juego->save();
	    	return redirect('juegos/ver/'.$request->id);
	    }



	}
 ?>