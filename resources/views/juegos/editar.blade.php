@extends('layouts.master')
@section('titulo')
	Editar Juego
@endsection
@section('contenido')
<div class="row">
	 <div class="offset-md-3 col-md-6">
		 <div class="card">
			 <div class="card-header text-center">
			 	Editar Juego
			 </div>
			 <div class="card-body" style="padding:30px">
			 	<form method="post" action="{{url('juegos/editar')}}/{{$juego->id}}" enctype="multipart/form-data">
			 		{{ csrf_field() }}
					 <div class="form-group">
						 <label for="titulo">Titulo</label>
						 <input type="text" name="titulo" id="titulo" class="form-control" value="{{$juego->titulo}}">
					 </div>
					 <div class="form-group">
						 <label for="votos_positivos">votos_positivos</label>
						 <input type="number" name="votos_positivos" id="votos_positivos" min="0" class="form-control" value="{{$juego->votos_positivos}}">
					 </div>
					 <div class="form-group">
						 <label for="votos_negativos">votos_negativos</label>
						 <input type="number" name="votos_negativos" id="votos_negativos" min="0" class="form-control" value="{{$juego->votos_negativos}}">
					 </div>
					 <div class="form-group">
					    <label for="categoria">Categoria</label>
					    <select class="form-control" id="categoria">
					      @foreach($categorias as $categoria)
					      	<option value="{{$categoria->id}}">{{$categoria->categoria}}
					      @endforeach
					    </select>
					  </div>

					 
					 <div class="form-group">
						 <label for="descripcion">Descripcion</label>
						 <textarea name="descripcion" id="descripcion" class="form-control" rows="3">v{{$juego->descripcion}}</textarea>
					 </div>
					 <div class="form-group">
					 	<input type="file" name="imagen" id="imagen" >
					 </div>
					 <div class="form-group text-center">
						 <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
						 	Actualizar juego
						 </button>
					 </div>
				 </form>
			 </div>
		 </div>
	 </div>
	</div>
@endsection