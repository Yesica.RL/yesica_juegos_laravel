@extends('layouts.master')
@section('titulo')
	Mostrar Juego
@endsection
@section('contenido')
	<div class="row">
		
			<div class="col-sm-3 ">
				<img class="img-thumbnail " src="{{asset('assets/imagenes')}}/{{$juego->imagen}}">
			</div>
			<div class="col-sm-9">
				<h2>{{$juego->titulo}}</h2><br>
				<p>{{$juego->descripcion}}</p>
				<h4>
					
				</h4>
				<h3>Votos</h3>
				<p>{{$juego->votos_positivos}}<img src="{{asset('assets/imagenes')}}/positivo.png"><img src="{{asset('assets/imagenes')}}/negativo.png">{{$juego->votos_negativos}} </p>
				
				
				<a href="{{url('juegos/editar')}}/{{$juego->id}}" class="btn btn-danger">Editar</a>
				<a href="{{url('/')}}" class="btn btn-danger">Volver al listado</a>
			</div>
		
	</div>
@endsection