@extends('layouts.master')
@section('titulo')
	Principal
@endsection
@section('contenido')
	<div class="row">
		@foreach( $juegos as $clave => $juego )
		<div class="col-xs-12 col-sm-6 col-md-3 ">
			<div class="card" style="width: 18rem;">
			  <img class="card-img-top" src="{{ asset('assets/imagenes') }}/{{$juego->imagen}}">
			  <div class="card-body">
			    <h5 class="card-title">{{$juego->titulo}}</h5>
			    <p class="card-text">{{$juego->descripcion}}</p>
			    <a href="{{url('juegos/editar')}}/{{$juego->id}}" class="btn btn-primary">Editar</a>
			    <a href="{{ url('/juegos/ver/' . $juego->id ) }}" class="btn btn-primary">Ver</a>
			  </div>
			</div>
		</div>
			
		@endforeach
	</div>
@endsection